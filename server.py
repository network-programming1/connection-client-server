import socket

#create a socket object
server_socket = socket.socket()        
print ("Socket successfully created")

#reserve a port on your computer in our
port = 12345

#bind to the port
server_socket.bind(('127.0.0.1', port))        
print ("socket binded to %s" %(port))

#put the socket into listening mode
server_socket.listen(5)    
print ("socket is listening")  

# Establish connection with client.
conn, addr = server_socket.accept()    
print ('Got connection from', addr )

number = 0
while number < 100:
    number += 1
    conn.send(str(number).encode())
    data = conn.recv(1024).decode()
    number = int(data)
    print(number)

print("Closing connection.")
conn.close()
server_socket.close()
