import socket

#create a socket object
client_socket = socket.socket()

#Define the port on which you want to connect
port = 12345  

#connect to the server on local computer
client_socket.connect(('127.0.0.1', port))

# receive data from the server and decoding to get the string.
print ("Socket successfully connected")

number = 1
while number < 100:
    data = client_socket.recv(1024).decode()
    if not data:
        break

    number = int(data)
    print(number)

    number += 1
    client_socket.send(str(number).encode())

print("Closing connection.")
client_socket.close()
     